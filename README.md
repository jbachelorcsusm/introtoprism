# IntroToPrism:  A Plain-Old Barebones Prism Xamarin.Forms App #

This app is what you might create using the [Creating a Xamarin.Forms App with Prism](https://drive.google.com/open?id=1ZMTDSwYSVzHgfbc3lIYej3fZYSTlpmDT) tutorial. If you have not yet gone through this tutorial, I would highly recommend doing so before relying on this completed version.

***

### What is this repository demonstrating?
* Finished version of converting a default blank Xamarin.Forms app to an MVVM Prism app using Unity for dependency injection.
* View the individual commits if you'd like to see the steps along the way.

***

### Holy moly... This is really complicated!
It's a rather large pill to swallow, going from a default app to a full-blown MVVM implementation including an MVVM framework (Prism) and a depedency injection container (Unity -- NOT the gaming engine/IDE). 

Don't worry, and don't stress... This all gets easier with practice, and we're going to get lots and lots of practice!

***

### Additional Resources
* [Brian Lagunas demos Prism with Xamarin.Forms on James Montemagno's Xamarin Show](https://channel9.msdn.com/Shows/XamarinShow/Prism-for-XamarinForms-with-Brian-Lagunas)
* [Brian Lagunas demos Prism with Xamarin.Forms](https://www.youtube.com/watch?v=DYRLcqG2BAY) at Xamarin's Evolved conference, 2016
* [Blog posts by Charlin Agramonte](https://xamgirl.com/prism-in-xamarin-forms-step-by-step-part-1/)
* [Prism sample code with Xamarin.Forms](https://github.com/PrismLibrary/Prism-Samples-Forms)
* [Prism documentation](http://prismlibrary.github.io/docs/xamarin-forms/Getting-Started.html) (warning... This is a work in progress, so you will likely find some sections that are incomplete, or not quite up to date)
* [Brian Lagunas' blog recent blog post on Prism 7](http://brianlagunas.com/whats-new-in-prism-for-xamarin-forms-7-0/) - This will get you some of the most up-to-date information available.
* [Upgrading to Prism 7](https://xamgirl.com/updating-prism-7-0-xamarin-forms/) by Charlin Agramonte - Again, this will be some very up-to-date information, and could prove very helpful.
* [StackOverflow](https://stackoverflow.com/questions/tagged/prism):  The major prism contributors are extremely responsive to questions on StackOverflow. There are numerous answers here.
* [Last but not least, your CS-481 class (me included)! Don't forget about the "Post your questions here" forum](https://cc.csusm.edu/mod/oublog/view.php?id=630348)