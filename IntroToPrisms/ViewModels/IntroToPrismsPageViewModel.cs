﻿using System.Diagnostics;
using Prism.Mvvm;

namespace IntroToPrisms.ViewModels
{
    public class IntroToPrismsPageViewModel : BindableBase
    {
        public IntroToPrismsPageViewModel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}:  ctor");
        }
    }
}
