﻿using System.Diagnostics;
using Xamarin.Forms;

namespace IntroToPrisms.Views
{
    public partial class IntroToPrismsPage : ContentPage
    {
        public IntroToPrismsPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}:  ctor");
            InitializeComponent();
        }
    }
}
